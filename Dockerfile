ARG BUILDTIME_FROM=rust:1.71
ARG RUNTIME_FROM=gcr.io/distroless/cc

FROM $BUILDTIME_FROM as build-env
RUN apt-get update && apt-get install build-essential cmake -y
RUN cargo install convco

FROM $RUNTIME_FROM
COPY --from=build-env /usr/local/cargo/bin/convco /usr/local/bin/convco
CMD ["/usr/local/bin/convco"]
